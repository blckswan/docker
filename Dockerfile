FROM node:12.18.1
ENV NODE_ENV=production
WORKDIR /npm

RUN npm install yarnpkg tap ibackuptool mocha --production

COPY . .

CMD [ "node", "server.js" ]
